
int main (string[] args) {
	var app = new Gtk.Application ("org.example.App", ApplicationFlags.FLAGS_NONE);
	app.activate.connect (() => {
		var win = app.active_window;
		if (win == null) {
			win = new ListDemoVala.Window (app);
		}
		win.present ();
	});

	return app.run (args);
}
